; --------------------- Drupal 6 platform.
; TODO: libraries, patches, modules with particular versions, custom modules
api = 2
core = 6.x

; --------------------- Core
projects[pressflow][type] = core
projects[pressflow][download][type] = get
projects[pressflow][download][url] = https://github.com/omega8cc/pressflow6/archive/pressflow-6.37.120.tar.gz

; See http://omega8.cc/supported-enabled-disabled-a-complete-list-150 for list of aegir included modules. This list DOES NOT include them. Only include if you want to use your own version.

; --------------------- Shared Platform Modules (latest versions used)
projects[] = ad
projects[] = admin
projects[] = admin_menu
projects[] = ajax_load
projects[] = ajaxblocks
projects[] = autoload
projects[] = better_formats
projects[] = calendar
projects[] = captcha
projects[] = cck
projects[] = colorbox
projects[] = ctm
projects[] = ctools
projects[] = custom_search
projects[] = disqus
projects[] = draggableviews
projects[] = easy_social
projects[] = ed_readmore
projects[] = editablefields
projects[] = email
projects[] = emfield
projects[] = extlink
projects[] = favicon
projects[] = filefield
projects[] = fivestar
projects[] = flickr
projects[] = globalredirect
projects[] = gmap
projects[] = google_analytics
projects[] = http_client
projects[] = ie6update
projects[] = image_resize_filter
projects[] = imageapi
projects[] = imagecache
projects[] = imagefield
projects[] = imce
projects[] = imce_wysiwyg
projects[] = insert
projects[] = insert_block
projects[] = jquery_ui
projects[] = jquery_ui_filter
projects[] = jquery_update
projects[] = libraries
projects[] = link
projects[] = location
projects[] = media_youtube
projects[] = menu_block
projects[] = menu_breadcrumb
projects[] = menu_position
projects[] = menutree
projects[] = mollom
projects[] = nice_menus
projects[] = node_clone
projects[] = node_gallery
projects[] = node_import
projects[] = nodeblock
projects[] = nodequeue
projects[] = override_node_options
projects[] = path_redirect
projects[] = pathauto
projects[] = phone
projects[] = private_download
projects[] = quicktabs
projects[] = recaptcha
projects[] = rules
projects[] = signwriter
projects[] = site_verify
projects[] = social-share
projects[] = special_menu_items
projects[] = tagadelic
projects[] = taxonomy_breadcrumb
projects[] = taxonomy_menu
projects[] = taxonomy_redirect
projects[] = themekey
projects[] = token
projects[] = view_unpublished
projects[] = votingapi
projects[] = webform
projects[] = webform_validation

; --------------------- Version Locked Modules (no patches)

projects[node_import] = 1.2
projects[panels] = 3.10
projects[views] = 2.16
projects[views_accordion] = 1.5
projects[views_bulk_operations] = 1.17
projects[views_groupby] = 1.0-rc2
projects[views_or] = 1.0-alpha1

projects[auto_nodetitle][type] = module
projects[auto_nodetitle][download][type] = git
projects[auto_nodetitle][download][revision] = 08a80c803bb6982aa29480c877f17be8ab57434c
projects[auto_nodetitle][download][branch] = 6.x-1.x

projects[better_exposed_filters][type] = module
projects[better_exposed_filters][download][type] = git
projects[better_exposed_filters][download][revision] = a842c6e27390622f3d57545a3b592aee98867613
projects[better_exposed_filters][download][branch] = 6.x-2.x

project[cnr][type] = module
project[cnr][download][type] = git
project[cnr][download][revision] = 03eeed71a40a36e7913d51035218ec2f27dc9ad0
project[cnr][download][branch] = 6.x-4.x

projects[luceneapi][type] = module
projects[luceneapi][download][type] = git
projects[luceneapi][download][revision] = 2b1d0ee65b84b93099b7c39f88d8326d1c410149
projects[luceneapi][download][branch] = 6.2-2.x

; --------------------- Patched Modules (all versions locked)

projects[custom_breadcrumbs][type] = module
projects[custom_breadcrumbs][version] = 1.5
projects[custom_breadcrumbs][patch][] = https://bitbucket.org/treecareindustryassociation/tciadrupalplatforms/raw/44a0bf72da52e0a5378dc9a4a7162d10d46569ae/makefiles/patches/6/tcia-custom_breadcrumbs.patch

projects[date][type] = module
projects[date][version] = 2.9
projects[date][patch][] = https://bitbucket.org/treecareindustryassociation/tciadrupalplatforms/raw/44a0bf72da52e0a5378dc9a4a7162d10d46569ae/makefiles/patches/6/tcia-date.patch

projects[hierarchical_select][type] = module
projects[hierarchical_select][version] = 3.8
projects[hierarchical_select][patch][] = https://bitbucket.org/treecareindustryassociation/tciadrupalplatforms/raw/44a0bf72da52e0a5378dc9a4a7162d10d46569ae/makefiles/patches/6/tcia-hierarchical_select.patch
projects[hierarchical_select][patch][] = https://www.drupal.org/files/issues/2086955-select-parent-after-selecting-child-D6.patch

projects[htmlpurifier][type] = module
projects[htmlpurifier][version] = 2.4
projects[htmlpurifier][patch][] = https://bitbucket.org/treecareindustryassociation/tciadrupalplatforms/raw/44a0bf72da52e0a5378dc9a4a7162d10d46569ae/makefiles/patches/6/expo-htmlpurifier.patch

projects[pdf_to_imagefield][type] = module
projects[pdf_to_imagefield][version] = 2.0
projects[pdf_to_imagefield][patch][] = https://bitbucket.org/treecareindustryassociation/tciadrupalplatforms/raw/44a0bf72da52e0a5378dc9a4a7162d10d46569ae/makefiles/patches/6/tcia-pdf_to_imagefield.patch

projects[views_slideshow][type] = module
projects[views_slideshow][version] = 2.3
projects[views_slideshow][patch][] = https://bitbucket.org/treecareindustryassociation/tciadrupalplatforms/raw/44a0bf72da52e0a5378dc9a4a7162d10d46569ae/makefiles/patches/6/tcia-views_slideshow.patch

projects[views_hacks][type] = module
projects[views_hacks][version] = 1.0-beta2
projects[views_hacks][patch][] = https://bitbucket.org/treecareindustryassociation/tciadrupalplatforms/raw/44a0bf72da52e0a5378dc9a4a7162d10d46569ae/makefiles/patches/6/tcia-views_hacks.patch

projects[viewsdisplaytabs][type] = module
projects[viewsdisplaytabs][version] = 1.0-beta6
projects[viewsdisplaytabs][patch][] = https://bitbucket.org/treecareindustryassociation/tciadrupalplatforms/raw/44a0bf72da52e0a5378dc9a4a7162d10d46569ae/makefiles/patches/6/tcia-viewsdisplaytabs.patch

projects[wysiwyg][type] = module
projects[wysiwyg][download][type] = git
projects[wysiwyg][download][revision] = 053a9e7ca3a3978dd95223c5a809d20487cdc521
projects[wysiwyg][download][branch] = 6.x-2.x
projects[wysiwyg][patch][] = https://bitbucket.org/treecareindustryassociation/tciadrupalplatforms/raw/44a0bf72da52e0a5378dc9a4a7162d10d46569ae/makefiles/patches/6/tcia-wysiwyg-ckeditor-css.patch

projects[xmlsitemap][type] = module
projects[xmlsitemap][version] = 2.0
projects[xmlsitemap][patch][] = https://www.drupal.org/files/issues/xmlsitemap-remove-filesize-2220707-50-D6-do-not-test.patch

; Oauth is screwed, grab the 6.x-3.0-beta4 dev, revert it to working with patch.
projects[oauth][type] = module
projects[oauth][download][type] = git
projects[oauth][download][revision] = d66903a8e688b94588e0678f605fa5874ea43f87
projects[oauth][download][branch] = 6.x-3.x
projects[oauth][patch][] = https://bitbucket.org/treecareindustryassociation/tciadrupalplatforms/raw/44a0bf72da52e0a5378dc9a4a7162d10d46569ae/makefiles/patches/6/tcia-oauth.patch
; The above patch is the below patches merged. Some caused drush make failures
; http://drupal.org/files/oauth-q_query_string_ignore-1264390-15-D6.patch
; http://drupal.org/files/980340-d6.patch the second portion of this patch is not applied, was like this in prod, leaving as-is.
; http://drupal.org/files/issues/oauth-destination.patch
; http://drupal.org/files/oauth-authorization_edit-1338102-3-D6.patch
; http://drupal.org/files/oauth-oauth_verify-requirement-1538352-1-D6.patch
; http://drupal.org/files/oauth-oauth_common_get_user_provider_tokens-1431882-4-D6.patch

; Services is screwed, grab the latest dev, revert it to working with patch.
; Note that Services module in D6 is now unsupported becuase nobody wants to maintain it.
projects[services][type] = module
projects[services][download][type] = git
projects[services][download][revision] = 06ed9a0b43acd305b78db4af0cbb4ffa23032630
projects[services][download][branch] = 6.x-3.x
projects[services][patch][] = https://bitbucket.org/treecareindustryassociation/tciadrupalplatforms/raw/44a0bf72da52e0a5378dc9a4a7162d10d46569ae/makefiles/patches/6/tcia-services.patch

projects[twitter_pull][type] = module
projects[twitter_pull][version] = 1.4
projects[twitter_pull][patch][] = https://bitbucket.org/treecareindustryassociation/tciadrupalplatforms/raw/44a0bf72da52e0a5378dc9a4a7162d10d46569ae/makefiles/patches/6/expo-twitter_pull.patch

projects[nopremium][type] = module
projects[nopremium][download][type] = git
projects[nopremium][download][revision] = 94214b7d5be0edb1aadfa4906e11bc820d64ce94
projects[nopremium][download][branch] = 6.x-1.x
projects[nopremium][patch][] = https://bitbucket.org/treecareindustryassociation/tciadrupalplatforms/raw/44a0bf72da52e0a5378dc9a4a7162d10d46569ae/makefiles/patches/6/tcia-nopremium-allow-formats.patch

; --------------------- Libraries

libraries[htmlpurifier][download][type] = file
libraries[htmlpurifier][download][url] = http://htmlpurifier.org/releases/htmlpurifier-4.7.0.zip
libraries[htmlpurifier][directory_name] = htmlpurifier
libraries[htmlpurifier][destination] = libraries

libraries[plupload][download][type] = file
libraries[plupload][download][url] = https://github.com/moxiecode/plupload/archive/1.5.6.zip
libraries[plupload][destination] = libraries
libraries[plupload][directory_name] = plupload
;libraries[plupload][patch][] = http://drupal.org/files/plupload-1_5_6-rm_examples-1903850-5.patch

libraries[spyc][download][type] = file
libraries[spyc][download][url] = http://spyc.googlecode.com/files/spyc-0.5.zip
libraries[spyc][destination] = libraries
libraries[spyc][directory_name] = spyc

libraries[ckeditor][download][type] = file
libraries[ckeditor][download][url] = http://download.cksource.com/CKEditor/CKEditor/CKEditor%204.4.1/ckeditor_4.4.1_full.zip
libraries[ckeditor][destination] = libraries
libraries[ckeditor][directory_name] = ckeditor

libraries[tinymce][download][type] = file
libraries[tinymce][download][url] = http://download.moxiecode.com/tinymce/tinymce_3.5.8.zip
libraries[tinymce][destination] = libraries
libraries[tinymce][directory_name] = tinymce

libraries[colorbox][download][type] = file
libraries[colorbox][download][url] = http://drupal.org/files/colorbox.zip
libraries[colorbox][destination] = libraries
libraries[colorbox][directory_name] = colorbox

libraries[jquery.ui][download][type] = file
libraries[jquery.ui][download][url] = http://jquery-ui.googlecode.com/files/jquery-ui-1.7.3.zip
libraries[jquery.ui][destination] = libraries
libraries[jquery.ui][directory_name] = jquery.ui

libraries[luceneapi][download][type] = file
libraries[luceneapi][download][url] = http://downloads.sourceforge.net/project/luceneapi/luceneapi/6.x-2.0/luceneapi-lib-6.x-2.0.tar.gz?r=&ts=1369932199&use_mirror=hivelocity
libraries[luceneapi][destination] = modules/luceneapi
libraries[luceneapi][directory_name] = lib

libraries[twitter-api-php][download][type] = git
libraries[twitter-api-php][download][url] = git@github.com:J7mbo/twitter-api-php.git
libraries[twitter-api-php][destination] = libraries
libraries[twitter-api-php][directory_name] = twitter-api-php

; Themes
projects[] = tao
projects[] = rubik

projects[omega][type] = theme
projects[omega][download][type] = git
projects[omega][download][revision] = 5bd4c521b0a2f101ca5e608d713d140a55c7a127
projects[omega][download][branch] = 6.x-1.x

projects[ninesixty][type] = theme
projects[ninesixty][download][type] = git
projects[ninesixty][download][revision] = fca50e1cbd50409683819c79b279b1347f72b006
projects[ninesixty][download][branch] = 6.x-1.x
