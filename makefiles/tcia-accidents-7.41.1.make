; This file Is for the Accidents Drupal 7 website. Note that this site lives in a subdirectory of tcia.org
; see https://o4419664333.v104a.va.host8.biz/node/add/site for details of how this is done.

core = 7.x
api = 2

; --------------------- Core
projects[drupal][type] = core
projects[drupal][download][type] = get
projects[drupal][download][url] = https://github.com/omega8cc/7x/archive/7.41.1.tar.gz

; See http://omega8.cc/supported-enabled-disabled-a-complete-list-150 for list of aegir included modules. This list DOES NOT include them. Only include if you want to use your own version.

; --------------------- Shared Platform Modules (latest versions used)

projects[] = addressfield
projects[] = admin_menu
projects[] = apachesolr
projects[] = apachesolr_views
projects[] = auto_nodetitle
projects[] = ctools
projects[] = cck
projects[] = ckeditor
projects[] = facetapi
projects[] = date
projects[] = entitycache
projects[] = facetapi_slider
projects[] = feeds
projects[] = feeds_tamper
projects[] = globalredirect
projects[] = panels
projects[] = job_scheduler
projects[] = libraries
projects[] = menu_block
projects[] = nice_menus
projects[] = pathauto
projects[] = token
projects[] = views_data_export

; --------------------- Version Locked Modules (no patches)

projects[charts][type] = module
projects[charts][download][type] = git
projects[charts][download][url] = http://git.drupal.org/project/charts.git
projects[charts][download][branch] = 7.x-2.x
projects[charts][download][revision] = 050670fd54890cbf76215c7f49e9304cd331ded6

projects[jquery_update][type] = module
projects[jquery_update][download][type] = git
projects[jquery_update][download][branch] = 7.x-3.x
projects[jquery_update][download][revision] = b5e3ab0f2614a3c9b8b2f06e0627d5f2fda61ead

; --------------------- Version Locked Modules (patched)

projects[views][type] = module
projects[views][download][type] = git
projects[views][download][branch] = 7.x-3.13
projects[views][download][revision] = 5c2bfc9638e3e78023d1efb754b79d4ac994d0c6
projects[views][patch][] = https://www.drupal.org/files/1995056-Fatal-error-4_0.patch

; --------------------- Themes

projects[accidents][type] = theme
projects[accidents][download][type] = git
projects[accidents][download][url] = git@bitbucket.org:treecareindustryassociation/t.accidents.accidents.git

; --------------------- Libraries

libraries[highcharts][download][type] = file
libraries[highcharts][download][url] = http://code.highcharts.com/zips/Highcharts-4.1.9.zip
libraries[highcharts][directory_name] = highcharts
libraries[highcharts][type] = library

